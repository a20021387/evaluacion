﻿using Farmacia.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Modelos
{
    public class VentaCompletaModel
    {
        public Ventas Venta { get; set; }
        public List<ProductosVendidosCompletos> ProductosVendidos { get; set; }
        public float TotalDeVenta { get; set; }
    }
}
