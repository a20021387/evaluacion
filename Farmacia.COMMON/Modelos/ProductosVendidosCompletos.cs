﻿using Farmacia.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Modelos
{
    public class ProductosVendidosCompletos
    {
        public Ventas ProductoVendido { get; set; }
        public Ventas Producto { get; set; }
        public Ventas Cantidad { get; set; }
        public Ventas Precio { get; set; }
        public Ventas Sobrantes { get; set; }
        public float Total { get; set; }
    }
}       
