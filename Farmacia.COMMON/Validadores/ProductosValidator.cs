﻿using Farmacia.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Validadores
{
    public class ProductosValidator:AbstractValidator<Productos>
    {
        public ProductosValidator()
        {
            RuleFor(p => p.Nombre_Comercial).NotNull().NotEmpty().Length(1, 50);
            RuleFor(p=> p.Sustancia).Length(1, 50);
            RuleFor(p => p.Piezas).NotNull().NotEmpty();
            RuleFor(p => p.Lote).NotNull().NotEmpty().Length(1,50);
            RuleFor(p => p.Caducidad_Lote);
            RuleFor(p => p.Proveedor);
            RuleFor(p => p.Precio).NotNull().GreaterThan(0).NotEmpty();
            RuleFor(p => p.Numero_Factura);
            RuleFor(p => p.Fecha_Ingreso).NotNull().NotEmpty();
            RuleFor(p => p.Categoria).NotNull().NotEmpty();
        }
    }
}
