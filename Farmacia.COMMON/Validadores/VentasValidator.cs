﻿using Farmacia.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Validadores
{
    public class VentasValidator:AbstractValidator<Ventas>
    {
        public VentasValidator()
        {
            RuleFor(v=> v.Producto).NotNull().NotEmpty().Length(0,50);
            RuleFor(v => v.Cantidad_Vendida).NotNull().NotEmpty().GreaterThan(0);
            RuleFor(v => v.Piezas_Sobrantes).NotNull().NotEmpty();
            RuleFor(v => v.Precio).NotNull().NotEmpty().GreaterThan(0);
            RuleFor(v => v.Empleado).NotNull().NotEmpty();
            RuleFor(v => v.FechaHora).NotNull().NotEmpty();
            RuleFor(v => v.Id_Venta).NotNull().NotEmpty();
        }
    }
}
