﻿using Farmacia.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Validadores
{
    public class EmpleadoValidator:AbstractValidator<Empleado>
    {
        public EmpleadoValidator()
        {
            RuleFor(u => u.Matricula).NotNull().NotEmpty();
            RuleFor(u => u.Nombre).NotNull().NotEmpty().Length(1, 50);
            RuleFor(u => u.Apellido_Paterno).NotNull().NotEmpty().Length(1, 50);
            RuleFor(u => u.Apellido_Materno).NotNull().NotEmpty().Length(1, 50);
            RuleFor(u => u.Sueldo_Semanal).NotNull().NotEmpty(); 
        }
    }
}
