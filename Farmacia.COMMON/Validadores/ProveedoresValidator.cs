﻿using Farmacia.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Validadores
{
    public class ProveedoresValidator:AbstractValidator<Proveedores>
    {
        public ProveedoresValidator()
        {
            RuleFor(a=> a.Id_Provedor).NotNull().NotEmpty();
            RuleFor(a=> a.Nombre).NotNull().NotEmpty().Length(0,50);
            RuleFor(a=> a.Contacto).NotNull().NotEmpty().Length(0,50);
            RuleFor(a=> a.Telefono).NotNull().NotEmpty().Length(0,50);
        }
    }
}
