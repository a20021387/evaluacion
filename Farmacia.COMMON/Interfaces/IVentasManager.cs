﻿using Farmacia.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Interfaces
{
    /// <summary>
    /// Proporciona metodos relacionados a las ventas
    /// </summary>

    public interface IVentasManager:IGenericManager<Ventas>
    {
        /// <summary>
        /// Obtiene todas las ventas en el intervalo especificado
        /// </summary>
        /// <param name="inicio"></param>
        /// <param name="fin"></param>
        /// <returns></returns>
        IEnumerable<Ventas> VentasEnIntervalo(DateTime inicio, DateTime fin);
      
       /// <summary>
       /// Obtiene todas las ventas por empleado en una fecha especifica 
       /// </summary>
       /// <param name="matricula"></param>
       /// <param name="fecha"></param>
       /// <returns>listado de ventas realizadas</returns>
        IEnumerable<Ventas> VentasPorEmpleado(int matricula, DateTime fecha);
       
        
    }
}
