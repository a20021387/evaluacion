﻿using Farmacia.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Interfaces
{
    /// <summary>
    /// Proporciona los metodos relacionados a los productos
    /// </summary>
    public interface IProductosManager:IGenericManager<Productos>
    {
        IEnumerable<Productos> BuscarProductosPorNombre(string criterio);

        Productos BuscarProductoPorNombreExacto(string nombre);
    }
}
