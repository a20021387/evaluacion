﻿using Farmacia.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;


namespace Farmacia.COMMON.Interfaces
{
    /// <summary>
    /// Proporciona los metodos relacionados a los usuarios
    /// </summary>
    public interface IUsuarioManager:IGenericManager<Empleado>
    {
        /// <summary>
        /// Verifica si las credenciales son validas para el usuario
        /// </summary>
        /// <param name="NombreUser">Nombre del empleado</param>
        /// <param name="MatriculaUser">MAtricula o contraseña del usuario</param>
        /// <returns>Si las credenciales son correctars regresa el usuario completo, de otro modo regresa null</returns>
        Empleado Login(string NombreUser, int MatriculaUser);


    }
}
