﻿using Farmacia.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Interfaces
{
    /// <summary>
    /// Proporciona los metodos básicos de acceso a una tabla de base de datos
    /// </summary>
    /// <typeparam name="T">Tipo de entidad (clase) a la que se refiere una tabla</typeparam>
    public interface IGenericRepository<T> where T:BaseDTO
    {
        /// <summary>
        /// Proporciona información sobre el error ocurrido en alguna de las operaciones
        /// </summary>
       int Error { get; set; }
        /// <summary>
        /// Inserta una entidad en la tabla
        /// </summary>
        /// <param name="entidad">Entidad a insertar</param>
        /// <returns>Confirmación de la inserción</returns>
        bool Create(T entidad);
        /// <summary>
        /// Obtiene todos los registros de la tabla
        /// </summary>
        IEnumerable<T> Read { get; }
        /// <summary>
        /// Actualiza un registro en la tabla en base a la propiedad Id
        /// </summary>
        /// <param name="entidad">Entidad ya modificada, el Id debe existir en la tabla para modificarse</param>
        /// <returns>Confirmación de la actualización</returns>
        bool Update(T entidad);
        /// <summary>
        /// Elimina una entidad en la base de datos de acuerdo al id relacionado 
        /// </summary>
        /// <param name="id">Id de la entidad a eliminar</param>
        /// <returns>Confirmación de la eliminación</returns>
        bool Delete(string id);

        //Query---> Consultas de acuerdo a la tabla, mediante expresiones lamda
        /// <summary>
        /// Realiza una consulta personalizada a la tabla
        /// </summary>
        /// <param name="predicado">Expresión lamda que define la consulta</param>
        /// <returns>Conjunto de entidades que cumplen con la consulta</returns>
        IEnumerable<T> Query(Expression<Func<T, bool>> predicado);
        /// <summary>
        /// Obtener una entidad en base a su Id
        /// </summary>
        /// <param name="id">El Id de la entidad a obtener</param>
        /// <returns>Entidad completa que le corresponde el Id prporcionado</returns>
        T SearchById(string id);





    }
}
