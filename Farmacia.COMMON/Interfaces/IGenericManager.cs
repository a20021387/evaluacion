﻿using Farmacia.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Interfaces
{
    /// <summary>
    /// Proporciona metodos estandarizados para el acceso a tablas; cada manager creado debe implemenetar de esta interface
    /// </summary>
    /// <typeparam name="T">El tipo de entidad de la cual se implementa el Manager</typeparam>

    public interface IGenericManager<T> where T:BaseDTO
    {
        /// <summary>
        /// Proporciona el error relacionado despues de alguna operación
        /// </summary>
        string Error { get; }
        /// <summary>
        /// Inserta una entidad en la tabla
        /// </summary>
        /// <param name="entidad">Entidad a insertar</param>
        /// <returns>Confirmación de la inserción</returns>
        bool Insertar(T entidad);
        /// <summary>
        /// Obtiene todos los registros de la tabla
        /// </summary>
        IEnumerable<T> ObtenerTodos { get; }
        /// <summary>
        /// Actualiza un registro en la tabla en base a su propiedad Id
        /// </summary>
        /// <param name="entidad">Entidad ya modificada, el Id debe existir en la tabla</param>
        /// <returns> Confirmación de la actualización</returns>
        bool Actualizar(T entidad);
        /// <summary>
        /// Elimina una entidad en base al Id Proporcionado
        /// </summary>
        /// <param name="entidad">Id de la entidad a eliminar</param>
        /// <returns>Confirmación de la eliminación</returns>
        bool Eliminar(string id);
        /// <summary>
        /// Obtienen un elemento de acuerdo a su Id
        /// </summary>
        /// <param name="id"> Id del elemento a obtener</param>
        /// <returns>Entidad completa correspondiente al Id proporcionado</returns>
        T BuscarPorId(string id);


    }
}
