﻿using Farmacia.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Interfaces
{
    /// <summary>
    /// Proporciona los metodos relacionados a los proveedores
    /// </summary>
    public interface IProveedoresManager:IGenericManager<Proveedores>
    {
        /// <summary>
        /// Obtiene la información completa de un proveedor de acuerdo al Id dado   
        /// </summary>
        /// <param name="criterio">Id del proveedor</param>
        /// <returns>información de un proveedor</returns>
        IEnumerable<Proveedores> BuscarProveedorPorId(int id);

    }
}
