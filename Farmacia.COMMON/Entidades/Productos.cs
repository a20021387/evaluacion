﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Entidades
{
    public  class Productos:BaseDTO
    {
        public string Nombre_Comercial { get; set; }
        public string Sustancia { get; set; }
        public int Piezas { get; set; }
        public string Lote { get; set; }
        public string Caducidad_Lote { get; set; }
        public int Proveedor { get; set; }
        public int Precio { get; set; }
        public string Numero_Factura { get; set; }
        public string Fecha_Ingreso { get; set; }
        public int Categoria { get; set; }
    }
}
