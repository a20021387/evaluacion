﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Entidades
{
    public class Ventas:BaseDTO
    {
        public string Producto { get; set; }
        public int Cantidad_Vendida { get; set; }
        public int Piezas_Sobrantes { get; set; }
        public int Precio { get; set; }
        public int Empleado { get; set; }
        public DateTime FechaHora { get; set; }
        public int Id_Venta { get; set; }
    }
}
