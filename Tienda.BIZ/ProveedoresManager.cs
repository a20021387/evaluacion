﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda.BIZ
{
    public class ProveedoresManager : GenericManager<Proveedores>, IProveedoresManager
    {
        public ProveedoresManager(IGenericRepository<Proveedores> repositorio) : base(repositorio)
        {
        }

        public IEnumerable<Proveedores> BuscarProveedorPorId(int id)
        {
            return repository.Query(p=>p.Id_Provedor==id);
        }
    }
}
