﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda.BIZ
{
    public class EmpleadosManager : GenericManager<Empleado>, IUsuarioManager
    {
        public EmpleadosManager(IGenericRepository<Empleado> repositorio) : base(repositorio)
        {
        }

        public Empleado Login(string NombreUser, int MatriculaUser)
        {
            return repository.Query(u => u.Nombre == NombreUser && u.Matricula == MatriculaUser).SingleOrDefault();
        }
    }
}
