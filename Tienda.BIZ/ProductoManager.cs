﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda.BIZ
{
    public class ProductoManager : GenericManager<Productos>, IProductosManager
    {
        public ProductoManager(IGenericRepository<Productos> repositorio) : base(repositorio)
        {

        }

        public Productos BuscarProductoPorNombreExacto(string nombre)
        {
            return repository.Query(p => p.Nombre_Comercial == nombre).SingleOrDefault();
        }

        public IEnumerable<Productos> BuscarProductosPorNombre(string criterio)
        {
            return repository.Query(p => p.Nombre_Comercial.ToLower().Contains(criterio.ToLower()));
        }
    }
}
