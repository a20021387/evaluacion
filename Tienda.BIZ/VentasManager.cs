﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda.BIZ
{
    internal class VentasManager : GenericManager<Ventas>, IVentasManager
    {
        public VentasManager(IGenericRepository<Ventas> repositorio) : base(repositorio)
        {
        }


        public IEnumerable<Ventas> VentasEnIntervalo(DateTime inicio, DateTime fin)
        {
            DateTime rInicio = new DateTime(inicio.Year, inicio.Month, inicio.Day, 0, 0, 0);
            DateTime rFin = new DateTime(fin.Year, fin.Month, fin.Day, 0, 0, 0).AddDays(1);
            return repository.Query(v => v.FechaHora >= rInicio && v.FechaHora < rFin);
        }

       
        public IEnumerable<Ventas> VentasPorEmpleado(int matricula, DateTime fecha)
        {
            DateTime date = new DateTime(fecha.Year, fecha.Month, fecha.Day, 0, 0, 0);
            return repository.Query(v =>v.Empleado==matricula && v.FechaHora==fecha);
        }
    }
}
