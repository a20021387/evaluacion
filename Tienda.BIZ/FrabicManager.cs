﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using Farmacia.COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Tienda.BIZ
{
    public class FrabicManager
    {
        string Origen;

        public FrabicManager(string Origen)
        {
            this.Origen = Origen;
        }
        public IUsuarioManager UsuarioManager()
        {
            switch (Origen)
            {
                case "MSSQL":
                    return new EmpleadosManager(new Farmacia.DAL.SQL.GenericRepository<Empleado>(new EmpleadoValidator()));
                   
                default:
                    return null;
            }

        }

        public IProductosManager ProductosManager()
        {
            switch (Origen)
            {
                case "MSSQL":
                    return new ProductoManager(new Farmacia.DAL.SQL.GenericRepository<Productos>(new ProductosValidator(), false));

                default:
                    return null;
            }
        }

        public IVentasManager VentasManager()
        {
            switch (Origen)
            {
                case "MSSQL":
                    return new VentasManager(new Farmacia.DAL.SQL.GenericRepository<Ventas>(new VentasValidator(), false));

                default:
                    return null;
            }
        }

        public IProveedoresManager ProveedoresManager()
        {
            switch (Origen)
            {
                case "MSSQL":
                    return new ProveedoresManager(new Farmacia.DAL.SQL.GenericRepository<Proveedores>(new ProveedoresValidator(), false));

                default:
                    return null;
            }
        }


    }
}
