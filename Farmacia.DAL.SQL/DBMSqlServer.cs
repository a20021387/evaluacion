﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Farmacia.COMMON.Interfaces;

namespace Farmacia.DAL.SQL
{
    public class DBMSqlServer:IDB
    {
        SqlConnection connection;
        public string Error { get; private set; }

        public DBMSqlServer()
        {
        string server = @"LAPTOP-766VA1BO\SQLEXPRESS";
        string db = "Farmacia_de_Jesus";
        string user = "farmaciauser";
        string pass = "far123";
            connection = new SqlConnection($"Data Source={server};Initial Catalog={db};Persist Security Info=True;User ID={user};Password={pass}");
            conectar();

        }

        private bool conectar()
        {
            try
            {
                connection.Open();
                Error = "";
                return true;

            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;

            }
        }

        public bool Comando(string command)
        {
            try
            {
                SqlCommand cmd=new SqlCommand(command,connection);
                cmd.ExecuteNonQuery();
                Error = "";
                return true;

            }
            catch (Exception ex)
            {

                Error=ex.Message;   
                return false;
            }

        }


        public object Consulta(string consulta)
        {
            //connection.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(consulta,connection);
                SqlDataReader dr=cmd.ExecuteReader();
                Error = "";
                return dr;

            }
            catch (Exception ex)
            {

                Error=ex.Message;
                return null;
            }

        }
    }
}
