﻿using Farmacia.COMMON.Entidades;
using Farmacia.UI.Modulos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Farmacia.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(Empleado usr)
        {
            InitializeComponent();
            lblUsuario.Content = $"{usr.Nombre} {usr.Apellido_Paterno} - {usr.Matricula}";
        }

        private void menuHome_Selected(object sender, RoutedEventArgs e)
        {
            MostrarContenido(null);

        }

        private void menuUsuarios_Selected(object sender, RoutedEventArgs e)
        {
            MostrarContenido(new UsuariosUserControl());

        }

        private void menuVentas_Selected(object sender, RoutedEventArgs e)
        {

        }

        private void menuProductos_Selected(object sender, RoutedEventArgs e)
        {
            MostrarContenido(new ProductosUserControl());

        }

        private void MostrarContenido(UserControl control)
        {
            Contenedor.Content = null;
            Contenedor.Content = control;
        }
    }
}
