﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tienda.BIZ;

namespace Farmacia.UI.Modulos
{
    /// <summary>
    /// Lógica de interacción para UsuariosUserControl.xaml
    /// </summary>
    public partial class UsuariosUserControl : UserControl
    {
        IUsuarioManager usuarioManager;
        bool esNuevo;
        public UsuariosUserControl()
        {
            InitializeComponent();
            usuarioManager = Tools.FabricManager.UsuarioManager();
            HabilitarCajas(false);
            ActualizarTabla();
        }

        

        private void HabilitarCajas(bool v)
        {
            contenedorCampos.IsEnabled = v;
            btnNurvo.IsEnabled = !v;
            btnEditar.IsEnabled = !v;
            btnGuardar.IsEnabled = v;
            btnEliminar.IsEnabled = !v;
            btnCancelat.IsEnabled = v;

        }
        private void btnNurvo_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;
            this.DataContext = new Empleado();
            HabilitarCajas(true);
            ActualizarTabla();


        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            Empleado prod = dtgDatos.SelectedItem as Empleado;
            if (prod != null)
            {

                this.DataContext = prod;
                esNuevo = false;
                HabilitarCajas(true);
            }
            else
            {
                MessageBox.Show("Primero Debe Seleccionar un Usuario", "Farmacia de Jesús", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (esNuevo)
            {
                if (usuarioManager.Insertar(this.DataContext as Empleado))
                {
                    MessageBox.Show("Usuario Insertado", "Farmacia de Jesús", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTabla();
                    HabilitarCajas(false);

                }
                else
                {
                    MessageBox.Show(usuarioManager.Error, "Farmacia de Jesús", MessageBoxButton.OK, MessageBoxImage.Error);

                }

            }
            else
            {
                if (usuarioManager.Actualizar(this.DataContext as Empleado))
                {
                    MessageBox.Show("Usuario Actualizado", "Farmacia de Jesús", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTabla();
                    HabilitarCajas(false);

                }
                else
                {
                    MessageBox.Show(usuarioManager.Error, "Farmacia de Jesús", MessageBoxButton.OK, MessageBoxImage.Error);

                }

            }


        }

        private void btnCancelat_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;
            this.DataContext = new Empleado();
            HabilitarCajas(false);

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Empleado usr = dtgDatos.SelectedItem as Empleado;
            if (usr != null)
            {
                if (MessageBox.Show("Desea Eliminar al Usuario " + usr.Nombre + "?", "Farmacia de Jesús", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (usuarioManager.Eliminar(usr.Nombre))
                    {
                        MessageBox.Show("Producto Eliminado Correctamente", "Farmacia de Jesús", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTabla();
                    }
                    else
                    {
                        MessageBox.Show(usuarioManager.Error, "Farmacia de Jesús", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                }


            }
            else
            {
                MessageBox.Show("Primero Debe Seleccionar un Usuario", "Farmacia de Jesús", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void ActualizarTabla()
        {
            this.DataContext = new Empleado();
            dtgDatos.ItemsSource = null;
            dtgDatos.ItemsSource = usuarioManager.ObtenerTodos;
        }
    }
}
