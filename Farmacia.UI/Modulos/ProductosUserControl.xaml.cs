﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Farmacia.UI.Modulos
{
    /// <summary>
    /// Lógica de interacción para ProductosUserControl.xaml
    /// </summary>
    public partial class ProductosUserControl : UserControl
    {
        IProductosManager productosManager;
        bool esNuevo;
        public ProductosUserControl()
        {
            InitializeComponent();
            productosManager = Tools.FabricManager.ProductosManager();
            
            HabilitarCajas(false);
            ActualizarTabla();

        }

        private void HabilitarCajas(bool v)
        {
            contenedorCampos.IsEnabled = v;
            btnNurvo.IsEnabled = !v;
            btnEditar.IsEnabled = !v;
            btnGuardar.IsEnabled = v;
            btnEliminar.IsEnabled = !v;
            btnCancelat.IsEnabled = v;
        }

        private void btnNurvo_Click(object sender, RoutedEventArgs e)
        { 
            esNuevo = true;
            this.DataContext = new Productos();
            HabilitarCajas(true);
            ActualizarTabla();


        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            Productos prod = dtgDatos.SelectedItem as Productos;
            if (prod != null)
            {

                this.DataContext = prod;
                esNuevo = false;
                HabilitarCajas(true);
            }
            else
            {
                MessageBox.Show("Primero Debe Seleccionar un Producto", "Farmacia de Jesús", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (esNuevo)
            {
                if (productosManager.Insertar(this.DataContext as Productos))
                {
                    MessageBox.Show("Producto Insertado","Farmacia de Jesús",MessageBoxButton.OK,MessageBoxImage.Information);
                    ActualizarTabla();
                    HabilitarCajas(false);

                }
                else
                {
                    MessageBox.Show(productosManager.Error, "Farmacia de Jesús", MessageBoxButton.OK, MessageBoxImage.Error);

                }

            }
            else
            {
                if (productosManager.Actualizar(this.DataContext as Productos))
                {
                    MessageBox.Show("Producto Actualizado", "Farmacia de Jesús", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTabla();
                    HabilitarCajas(false);

                }
                else
                {
                    MessageBox.Show(productosManager.Error, "Farmacia de Jesús", MessageBoxButton.OK, MessageBoxImage.Error);

                }

            }


        }

        private void btnCancelat_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;
            this.DataContext = new Productos();
            HabilitarCajas(false);

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Productos prod = dtgDatos.SelectedItem as Productos;
            if (prod!=null)
            {
                if (MessageBox.Show("Desea Eliminar al Producto "+prod.Nombre_Comercial+"?","Farmacia de Jesús",MessageBoxButton.YesNo,MessageBoxImage.Question)==MessageBoxResult.Yes)
                {
                    if (productosManager.Eliminar(prod.Nombre_Comercial))
                    {
                        MessageBox.Show("Producto Eliminado Correctamente", "Farmacia de Jesús",MessageBoxButton.OK,MessageBoxImage.Information);
                        ActualizarTabla();
                    }
                    else
                    {
                        MessageBox.Show(productosManager.Error,"Farmacia de Jesús",MessageBoxButton.OK,MessageBoxImage.Error);
                    }

                }
                

            }
            else
            {
                MessageBox.Show("Primero Debe Seleccionar un Producto", "Farmacia de Jesús", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void ActualizarTabla()
        {
            this.DataContext = new Productos();
            dtgDatos.ItemsSource = null;
            dtgDatos.ItemsSource = productosManager.ObtenerTodos;
        }
    }
}
